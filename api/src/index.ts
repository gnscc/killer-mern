import app from './app'
import './config/database'
import config from './config/config'

app.listen(config.SERVE_PORT, () => {
    console.log(`server on port ${config.SERVE_PORT}`)
})