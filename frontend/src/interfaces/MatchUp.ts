export interface MatchUp {
    code:           string,
    killer:         string,
    victim?:        string,
    weapon?:        string,
    place?:         string,
    killedBy?:      string[],
    pastVictims?:   string[],
    pastWeapons?:   string[],
    pastPlacess?:   string[],

}