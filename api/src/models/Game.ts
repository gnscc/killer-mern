import { Schema, model } from "mongoose";

const MatchUp = new Schema ({
    killer:     { type: String, required: true },
    victim:     { type: String, required: true },
    weapon:     { type: String, required: true },
    place:      { type: String, required: true },
    killedBy:   { type: [String], required: true },
    pastVictims:{ type: [String] },
    pastWeapons:{ type: [String] },
    pastPlaces: { type: [String] }
})

const Game = new Schema ({
    code:       { type: String , unique: true, required: true, dropDups: true },
    isStarted:  { type: Boolean, required: true },
    players:    { type: [String] },
    weapons:    { type: [String] },
    places:     { type: [String] },
    matchUps:   { type: [MatchUp] }
}, {
    versionKey: false,
    timestamps: true
})

export default model('Game', Game)