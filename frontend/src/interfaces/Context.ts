import { Game } from "./Game";

export interface Context {
    game:       Game,
    setGame:    React.Dispatch<React.SetStateAction<Game>>
}