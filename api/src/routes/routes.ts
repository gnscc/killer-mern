import { Router } from 'express'
import * as api from '../controllers/apiController'

const router = Router()

router.post('/newGame', api.newGame)

router.post('/startGame', api.startGame)

router.post('/getMatchUp', api.getMatchUp)

router.post('/saveGame', api.saveGame)

router.post('/killAlert', api.killAlert)

export default router