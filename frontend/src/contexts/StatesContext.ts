import { createContext } from "react";
import { ContextStates } from "../interfaces/ContextStates";
import * as configStates from "../states/configGame"

export const StatesContext = createContext< ContextStates >({states: configStates.initialState(), setStates: () => {}})