export interface State {
    id:         number,
    selected:   boolean,
    text:       string[]
}