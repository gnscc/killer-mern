import { RequestHandler } from "express"
import Game from "../models/Game"
import arrayShuffle from "array-shuffle"

export const newGame: RequestHandler = async (req, res) => {
    try {
        let newGame = await Game.findOne({ code: req.body.code})
        if (!newGame) {
            const game = new Game(req.body)
            newGame = await game.save()
        }
        
        res.json(newGame)
    } catch (error) {
        res.json(error)
    }
}

export const startGame: RequestHandler = async (req, res) => {
    try {
        const players : String[] = arrayShuffle(req.body.players)
        const matchUps = generateMatchUps(players, req.body.weapons, req.body.places);
        const gameToStart = await Game.findOne({ code: req.body.code})
        gameToStart.players = players
        gameToStart.weapons = req.body.weapons
        gameToStart.places = req.body.places
        gameToStart.matchUps = matchUps
        gameToStart.isStarted = true

        const ret = await gameToStart.save()
        res.json(ret)
    } catch (error) {
        res.json(error)
    }
}

export const getMatchUp: RequestHandler = async (req, res) => {
    try {
        const game = await Game.findOne({ code: req.body.code})

        if (!game) return res.status(204).json()

        if (!game.isStarted) return res.status(204).json('Game Not Started Yet')

        const matchUp = game.matchUps.find ((mU: any) => mU.killer === req.body.killer)

        res.json(matchUp)
    } catch (error) {
        res.json(error)
    }
}

export const saveGame: RequestHandler = async (req, res) => {
    try {
        let newGame = await Game.findOne({ code: req.body.code})
        newGame.players = req.body.players
        newGame.weapons = req.body.weapons
        newGame.places = req.body.places

        await newGame.save()
        
        res.json(newGame)
    } catch (error) {
        res.json(error)
    }
}

export const killAlert: RequestHandler = async (req, res) => {
    try {
        let game = await Game.findOne({ code: req.body.code})

        // idVictimMatchUp & idKillerMatchUp
        let idV = game.matchUps.findIndex((matchUp: any) => matchUp.killer === req.body.killer)
        let idK = game.matchUps.findIndex((matchUp: any) => matchUp.victim === req.body.killer && matchUp.killedBy.length < 1)

        // Update victim has been killed by
        game.matchUps[idV].killedBy = [game.matchUps[idK].killer, game.matchUps[idK].weapon, game.matchUps[idK].place]

        // Update killer past victims, weapons and places
        game.matchUps[idK].pastVictims.push(game.matchUps[idK].victim)
        game.matchUps[idK].pastWeapons.push(game.matchUps[idK].weapon)
        game.matchUps[idK].pastPlaces.push(game.matchUps[idK].place)

        // Update killer new victim, weapon and place
        game.matchUps[idK].victim =  game.matchUps[idV].victim
        game.matchUps[idK].weapon = game.matchUps[idV].weapon
        game.matchUps[idK].place = game.matchUps[idV].place

        // Killer won
        if (game.matchUps[idK].killer===game.matchUps[idK].victim) {
            game.matchUps[idK].killedBy = [""]
        }

        await game.save()

        res.json(game.matchUps[idV])

    } catch (error) {
        res.json(error)
    }
}

const generateMatchUps = (players: String[], weapons: String[], places: String[]) => {
    let victimPlayers: String[] = [];
    let matchUps: {
        killer:     String,
        victim:     String,
        weapon:     String,
        place:      String,
        killedBy:   String[],
        pastVictims:String[]
        pastWeapons:String[],
        pastPlaces: String[]

    }[] = [];


    let shuffledPlayers: String[] = arrayShuffle(players.slice(1))
    
    victimPlayers = buildVictimsArray(0, players, shuffledPlayers, victimPlayers)

    weapons = arrayShuffle(weapons);
    places = arrayShuffle(places);

    players.forEach((player, idx) => {
        matchUps.push(
            {
                killer: player,
                victim: victimPlayers[idx],
                weapon: weapons[idx],
                place: places[idx],
                killedBy: [],
                pastVictims: [],
                pastWeapons: [],
                pastPlaces: []
            }
        )
    });
    
    return matchUps;
}

const isCircular = (players : String[], victims : String[]) : boolean => {

    let value : number = 0;
    for (let i = 0; i < players.length; ++i) {
        value = victims.findIndex((element) => element == players[value]);

        if (value === 0) {
            return (i === players.length - 1);
        }
    }

    return false;
}

const buildVictimsArray = (killerIdx : number, players : String[], shuffledPlayers : String[], victimPlayers : String[]) : String[] => {

    let victim : String = shuffledPlayers.shift() as String
    victimPlayers[killerIdx] = victim

    killerIdx = players.indexOf(victim)

    if (shuffledPlayers.length > 0) {
        victimPlayers = buildVictimsArray(killerIdx, players, shuffledPlayers, victimPlayers)
    } else {
        victimPlayers[killerIdx] = players[0]
    }

    return victimPlayers
}