import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import Routes from './routes/routes'

const app = express()

app.use(morgan('dev'))

app.use(cors())

app.use(express.json())

app.use(express.urlencoded({extended: false}))

app.use(Routes)

export default app