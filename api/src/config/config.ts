import dotenv from 'dotenv'

dotenv.config()

export default {
    MONGO_DATABASE: process.env.MONGO_DATABASE || 'database',
    MONGO_USER: process.env.MONGO_USER || "my_user",
    MONGO_PASSWORD: process.env.MONGO_PASSWORD || "my_password",
    MONGO_HOST: process.env.MONGO_HOST || 'database',
    MONGO_PORT: process.env.MONGO_PORT || 27017,
    SERVE_PORT: process.env.SERVE_PORT || 3000
};