import { State } from "./State";

export interface ContextStates {
    states:     State[],
    setStates:  React.Dispatch<React.SetStateAction<State[]>>
}