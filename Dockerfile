FROM node:latest

RUN mkdir /app

WORKDIR /app

COPY ./api /app

RUN npm install

RUN npm run build

CMD ["npm", "run", "prod"]