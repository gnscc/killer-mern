export interface Game {
    code:       string,
    isStarted:  boolean,
    players?:   string[],
    weapons?:   string[],
    places?:    string[],
    matchUps?:  Object[]
}