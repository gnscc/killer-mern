import { createContext } from "react";
import { Context } from "../interfaces/Context";

export const GameContext = createContext< Context >({game: {code: "", isStarted: false}, setGame: () => {}})